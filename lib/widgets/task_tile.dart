import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {

  TaskTile({this.isCheckValue,this.textValue,this.checkBoxCallback,this.LongPress});
  final bool isCheckValue;
  final String textValue;
  final Function checkBoxCallback;
  final Function LongPress;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: LongPress,
      title: Text(
        '$textValue',
        style: TextStyle(
            decoration: isCheckValue
                ? TextDecoration.lineThrough
                : TextDecoration.none),
      ),
      trailing: Checkbox(
        value: isCheckValue,
        onChanged: checkBoxCallback,
      ),
    );
  }
}



